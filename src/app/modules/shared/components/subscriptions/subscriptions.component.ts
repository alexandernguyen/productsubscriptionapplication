import {Component, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  template: ''
})
export class SubscriptionsComponent implements OnDestroy {
  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor() { }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
