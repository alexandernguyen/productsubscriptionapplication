import { Component } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  public displayMobileMenu = false;
  constructor() { }

  public toggleMobileMenu(): void {
    this.displayMobileMenu = !this.displayMobileMenu;
  }
}
