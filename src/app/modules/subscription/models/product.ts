import {ProductTypeEnum} from '../enum/product';
import {Plan} from './plan';

export interface Product {
  id: string;
  name: ProductTypeEnum;
  features: string[];
  plans: Plan[];
}
export interface Products {
  products: Product[];
}
