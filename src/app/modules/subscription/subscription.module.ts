import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent, SubscriptionComponent } from './components/';
import { SubscriptionRoutingModule } from './subscription-routing.module';
import {ProductRequestsService} from './services/';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    ProductComponent,
    SubscriptionComponent,
  ],
  imports: [
    SharedModule,
    SubscriptionRoutingModule,
    CommonModule,
  ],
  providers: [
    ProductRequestsService
  ]
})
export class SubscriptionModule { }
