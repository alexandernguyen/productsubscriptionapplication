import {Component} from '@angular/core';
import {PaymentTypeEnum} from '../../enum/';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent  {
  public paymentTypes = PaymentTypeEnum;
  public selectedPaymentType: PaymentTypeEnum = PaymentTypeEnum.MONTH;
  constructor() { }

  public changPaymentType(paymentType: PaymentTypeEnum): void {
    this.selectedPaymentType = paymentType;
  }
}
