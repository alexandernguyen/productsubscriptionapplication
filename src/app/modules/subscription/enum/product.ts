export enum ProductTypeEnum {
  SILVER = 'Silver',
  GOLD = 'Gold',
  ENTERPRISE = 'Enterprise',
  PLATINUM = 'Platinum',
}
